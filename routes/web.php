<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/register/influencer', 	['as' => 'register.influencer', 	'uses' => 'RegisterController@influencer']);
Route::get('/register/brand', 		['as' => 'register.brand', 			'uses' => 'RegisterController@brand']);

Route::group(['prefix' => 'backend/', 'as' => 'backend.', 'namespace' => 'Backend\\', 'middleware' => 'auth'], function(){

	Route::get('dashboard', ['as' => 'dashboard', 		'uses' => 'DashboardController@index']);

	//Post Routes
	Route::get('post', 						['as' => 'post', 				'uses' => 'PostController@index']);
	Route::get('post/create', 				['as' => 'post.create', 		'uses' => 'PostController@create']);
	Route::post('post/store', 				['as' => 'post.store', 			'uses' => 'PostController@store']);
	Route::get('post/view/{slug}', 			['as' => 'post.view', 			'uses' => 'PostController@view']);
	Route::get('post/edit/{slug}', 			['as' => 'post.edit', 			'uses' => 'PostController@edit']);
	Route::post('post/update/{slug}', 		['as' => 'post.update', 		'uses' => 'PostController@update']);
	Route::post('post/delete', 				['as' => 'post.delete', 		'uses' => 'PostController@delete']);

	//Category Routes
	Route::get('category', 						['as' => 'category', 				'uses' => 'CategoryController@index']);
	Route::get('category/create', 				['as' => 'category.create', 		'uses' => 'CategoryController@create']);
	Route::post('category/store', 				['as' => 'category.store', 			'uses' => 'CategoryController@store']);
	Route::get('category/view/{slug}', 			['as' => 'category.view', 			'uses' => 'CategoryController@view']);
	Route::get('category/edit/{slug}', 			['as' => 'category.edit', 			'uses' => 'CategoryController@edit']);
	Route::post('category/update/{slug}', 		['as' => 'category.update', 		'uses' => 'CategoryController@update']);
	Route::post('category/delete', 				['as' => 'category.delete', 		'uses' => 'CategoryController@delete']);

});

Route::group(['as' => 'frontend.', 'namespace' => 'Frontend\\'], function(){

	Route::get('/home', 		['as' => 'home', 		'uses' => 'HomeController@index']);

	//Post Routes
	Route::get('jobs', 						['as' => 'job', 				'uses' => 'JobController@index']);
	Route::get('jobs/{slug}', 				['as' => 'job.view', 			'uses' => 'JobController@view']);

	//Category Routes
	/*Route::get('category', 						['as' => 'category', 				'uses' => 'CategoryController@index']);
	Route::get('category/create', 				['as' => 'category.create', 		'uses' => 'CategoryController@create']);
	Route::post('category/store', 				['as' => 'category.store', 			'uses' => 'CategoryController@store']);
	Route::get('category/view/{slug}', 			['as' => 'category.view', 			'uses' => 'CategoryController@view']);
	Route::get('category/edit/{slug}', 			['as' => 'category.edit', 			'uses' => 'CategoryController@edit']);
	Route::post('category/update/{slug}', 		['as' => 'category.update', 		'uses' => 'CategoryController@update']);
	Route::post('category/delete', 				['as' => 'category.delete', 		'uses' => 'CategoryController@delete']);*/

});
