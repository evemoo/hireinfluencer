<script src="{{ asset('backend/global/scripts/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/scripts/notify.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.delete').on('click', function(){
            var $this = $(this);
            var $slug = $(this).data('slug');
            bootbox.confirm('Are you sure?', function(response){
                if (response == false) {
                    $.notify('canceled', 'success');
                }
                else
                {
                    $.ajax({
                        url: '{{ route($base_route.'.delete') }}',
                        method: 'POST',
                        data: {"slug":$slug, "_token":'{{ csrf_token() }}'},
                        success:function(response){
                            $.notify('Deleted', 'success');
                            $this.closest('tr').remove();
                        }
                    });
                }
            });
        });
    });
</script>