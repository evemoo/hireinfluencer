@extends('backend.layouts.master')

@section('content')

	<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            
            @include('backend.layouts.includes.primary_breadcrumb')

            @include('backend.layouts.includes.secondary_breadcrumb')
            
            <!-- END PAGE HEADER-->
            <div class="note note-info">
                <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>

@stop