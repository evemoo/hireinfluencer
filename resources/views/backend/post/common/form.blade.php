<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Post Title</label>
        <div class="col-md-6">
            {!! Form::text('post_title', null, ['class' => 'form-control input-circle', 'placeholder' => 'Remember!! A proper Title is very important!']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Post Description</label>
        <div class="col-md-6">
            {!! Form::textarea('description', null, ['class' => 'form-control input-circle', 'placeholder' => 'Describe your post. What is it about and what you expect form the influencer.']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Image</label>
        <div class="col-md-6">
            {!! Form::file('image', ['class' => 'form-control input-circle']) !!}
        </div>
    </div>
    @if(isset($data['row']) && $data['row']->image !== null)
    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-6">
            <img src="{{ asset('images/post/'. $data['row']->image) }}" width="300">
        </div>
    </div>
    @endif
    <div class="form-group">
        <label class="col-md-3 control-label">Email Address</label>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon input-circle-left">
                    <i class="fa fa-envelope"></i>
                </span>
                {!! Form::email('email', null, ['class' => 'form-control input-circle-right', 'placeholder' => 'Email Address']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Category</label>
        <div class="col-md-6">
            <select class="form-control input-circle" name="category_id">
                @foreach($data as $key => $cat)
                    <optgroup label="{{ $key }}">
                        @foreach($cat as $key => $c)
                            <option value="{{ $key }}">{{ $c }}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Bid Expiry Date</label>
        <div class="col-md-6">
            <div class="input-icon">
                <i class="fa fa-calendar"></i>
                {!! Form::text('bid_expiry', null, ['class' => 'form-control input-circle', 'id' => 'datepicker']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Currency</label>
        <div class="col-md-6">
            {!! Form::text('currency', null, ['class' => 'form-control input-circle', 'placeholder' => 'Currency Symbol']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Bid Range</label>
        <div class="col-md-6">
            {!! Form::number('budget_low', null, ['class' => 'form-control input-circle', 'placeholder' => 'Bid From']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-6">
            {!! Form::number('budget_high', null, ['class' => 'form-control input-circle', 'placeholder' => 'Bid To']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Publish</label>
        <div class="col-md-6">
            {!! Form::radio('status', '1', true) !!} Yes
            {!! Form::radio('status', '0', false) !!} No
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-circle green">Save</button>
            <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
        </div>
    </div>
</div>