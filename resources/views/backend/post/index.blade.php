@extends('backend.layouts.master')

@section('content')

	<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            
            @include('backend.layouts.includes.primary_breadcrumb')

            @include('backend.layouts.includes.secondary_breadcrumb')

            @if(session()->has('message'))
                @include('backend.layouts.includes.message')
            @endif
            
            <!-- END PAGE HEADER-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">All of your Posts</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route($base_route.'.create') }}">
                            <div class="btn-group btn-group-devided">
                                <label class="btn btn-transparent red btn-outline btn-circle btn-sm">
                                    <span><i class="icon-plus"></i> Add New</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th> Post Title </th>
                                    <th style="width: 30%;"> Description </th>
                                    <th> Category </th>
                                    <th> Email </th>
                                    <th> Bid Expiry </th>
                                    <th> Budget </th>
                                    <th> Published </th>
                                    <th>  </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['rows'] as $key => $row)
                                <tr>
                                    <td> {{ $row->post_id }} </td>
                                    <td> {{ $row->post_title }} </td>
                                    <td> {{ $row->description }} </td>
                                    <td> {{ $row->category_id }} </td>
                                    <td> {{ $row->email }} </td>
                                    <td> {{ $row->bid_expiry }} </td>
                                    <td> {{ $row->budget() }} </td>
                                    <td> {!! $row->publish() !!} </td>
                                    <td>
                                        <a href="{{ route($base_route.'.view', $row->slug) }}" class="btn btn-outline btn-circle red btn-sm blue">
                                                                <i class="fa fa-eye"></i></a>
                                        <a href="{{ route($base_route.'.edit', $row->slug) }}" class="btn btn-outline btn-circle btn-sm purple">
                                            <i class="fa fa-edit"></i></a>
                                        <a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm black delete" data-slug="{{ $row->slug }}">
                                                            <i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>

@stop

@section('js')

    @include('backend.common.delete-js')

@stop