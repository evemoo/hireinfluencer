@extends('backend.layouts.master')

@section('css')

    <link href="{{ asset('backend/global/css/blog.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('content')

	<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            
            @include('backend.layouts.includes.primary_breadcrumb')

            @include('backend.layouts.includes.secondary_breadcrumb')
            
            <!-- END PAGE HEADER-->
            
            <div class="blog-page blog-content-2">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="blog-single-content bordered blog-container">
                            <div class="blog-single-head">
                                <h1 class="blog-single-head-title">
                                    {{ $data['row']->post_title }}
                                    <a href="{{ route($base_route.'.edit', $data['row']->slug) }}" class="btn btn-outline btn-circle btn-sm purple">
                                            <i class="fa fa-edit"></i></a>
                                </h1>
                                <div class="blog-single-head-date" style="color: #a1a1a1; font-size: 24px;">
                                    {{ $data['row']->budget() }}
                                </div>
                            </div>
                            <div class="blog-single-img">
                                {!! $data['row']->image() !!} 
                            </div>
                            <div class="blog-single-desc">
                                <p>Post Id: {{ $data['row']->post_id }}</p>
                                <p>{{ $data['row']->description }}</p>
                            </div>
                            <div class="blog-single-foot">
                                <ul class="blog-post-tags">
                                    <li class="uppercase">
                                        <a href="javascript:;">{{ $data['row']->category_id }}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="blog-comments">
                                <h3 class="sbold blog-comments-title">Comments(30)</h3>
                                <div class="c-comment-list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" alt="" src="../assets/pages/img/avatars/team1.jpg"> </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="#">Sean</a> on
                                                <span class="c-date">23 May 2015, 10:40AM</span>
                                            </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" alt="" src="../assets/pages/img/avatars/team3.jpg"> </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="#">Strong Strong</a> on
                                                <span class="c-date">21 May 2015, 11:40AM</span>
                                            </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                        <img class="media-object" alt="" src="../assets/pages/img/avatars/team4.jpg"> </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <a href="#">Emma Stone</a> on
                                                        <span class="c-date">30 May 2015, 9:40PM</span>
                                                    </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" alt="" src="../assets/pages/img/avatars/team7.jpg"> </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="#">Nick Nilson</a> on
                                                <span class="c-date">30 May 2015, 9:40PM</span>
                                            </h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </div>
                                    </div>
                                </div>
                                <h3 class="sbold blog-comments-title">Leave A Comment</h3>
                                <form action="#">
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Name" class="form-control c-square"> </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Email" class="form-control c-square"> </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Website" class="form-control c-square"> </div>
                                    <div class="form-group">
                                        <textarea rows="8" name="message" placeholder="Write comment here ..." class="form-control c-square"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn blue uppercase btn-md sbold btn-block">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="blog-single-sidebar bordered blog-container">
                            <div class="blog-single-sidebar-search">
                                <div class="input-icon right">
                                    <i class="icon-magnifier"></i>
                                    <input type="text" class="form-control" placeholder="Search Blog"> </div>
                            </div>
                            <div class="blog-single-sidebar-recent">
                                <h3 class="blog-sidebar-title uppercase">Your Posts</h3>
                                <ul>
                                    @foreach($data['rows'] as $post)
                                    <li>
                                        <a href="{{ route('backend.post.view', $post->slug) }}">{{ $post->post_title }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>

@stop