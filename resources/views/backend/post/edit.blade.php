@extends('backend.layouts.master')

@section('content')

	<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            
            @include('backend.layouts.includes.primary_breadcrumb')

            @include('backend.layouts.includes.secondary_breadcrumb')
            
            <!-- END PAGE HEADER-->
            
            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Edit - {{ $data['row']->post_title }} </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            {!! Form::model($data['row'], ['route' => [$base_route.'.update', $data['row']->slug], 'method' => 'POST', 'class' => 'form-horizontal']) !!}

                                @include($base_route.'.common.form')

                            {!! Form::close() !!}
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>

@stop