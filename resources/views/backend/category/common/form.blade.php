<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Category Title</label>
        <div class="col-md-6">
            {!! Form::text('title', null, ['class' => 'form-control input-circle', 'placeholder' => 'Category Title Please!']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Parent</label>
        <div class="col-md-6">
            {!! Form::select('parent_id', $data['parents'], null, ['class' => 'form-control input-circle', 'placeholder' => 'Is Parent']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Status</label>
        <div class="col-md-6">
            {!! Form::radio('status', '1', true) !!} Active
            {!! Form::radio('status', '0', false) !!} Inactive
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-circle green">Save</button>
            <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
        </div>
    </div>
</div>