<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    
    @include('backend.layouts.partials.head')

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        
        <div class="page-wrapper">
            
            @include('backend.layouts.partials.header')

            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                
                @include('backend.layouts.partials.sidebar')

                <!-- BEGIN CONTENT -->

                @yield('content')
                
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            
            @include('backend.layouts.partials.footer')

        </div>
        <!-- END QUICK NAV -->
        
        @include('backend.layouts.partials.script')

        @yield('js')

    </body>

</html>