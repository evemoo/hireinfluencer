<!DOCTYPE html>
<html>
<head>
    <title>Recipe with Laravel 5.5 & VueJS</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="root"></div>
</body>
<script src="{{ mix('js/app.js') }}"></script>
</html>