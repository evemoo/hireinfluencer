@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('login') }}" role="form">
        {{ csrf_field() }}
      
        <h1>Sign In</h1>
        
        <fieldset>
          <legend><span class="number">1</span>Your basic info</legend>
          
          @if ($errors->has('email'))
              <span class="error">
                  <strong>*{{ $errors->first('email') }}</strong>
              </span>
          @endif
          <label for="mail">Email:</label>
          <input type="email" id="mail" name="email" value="{{ old('email') }}" required="" autofocus="">
          
          @if ($errors->has('password'))
              <span class="error">
                  <strong>*{{ $errors->first('password') }}</strong>
              </span>
          @endif
          <label for="password">Password:</label>
          <input type="password" id="password" name="password" required="">

          <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe" {{ old('remember') ? 'checked' : '' }}> Remember me

        </fieldset>

        <button type="submit">Sign In</button>
      </form>
@endsection