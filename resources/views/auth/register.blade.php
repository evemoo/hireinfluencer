@extends('layouts.app')

@section('content')

    <form action="{{ route('register') }}" method="POST" role="form" enctype="multipart/form-data">
        {{ csrf_field() }}
      
        <h1>Sign Up</h1>
        
        <fieldset>
          <legend><span class="number">1</span>Your basic info</legend>
          @if ($errors->has('full_name'))
              <span class="error">
                  <strong>*{{ $errors->first('full_name') }}</strong>
              </span>
          @endif
          <label for="name">Full Name:</label>
          <input type="text" id="full_name" name="full_name" value="{{ old('full_name') }}">

          @if ($errors->has('username'))
              <span class="error">
                  <strong>*{{ $errors->first('username') }}</strong>
              </span>
          @endif
          <label for="name">Username:</label>
          <input type="text" id="username" name="username" value="{{ old('username') }}">
          
          @if ($errors->has('email'))
              <span class="error">
                  <strong>*{{ $errors->first('email') }}</strong>
              </span>
          @endif
          <label for="mail">Email:</label>
          <input type="email" id="email" name="email" value="{{ old('email') }}">
          
          @if ($errors->has('password'))
              <span class="error">
                  <strong>*{{ $errors->first('password') }}</strong>
              </span>
          @endif
          <label for="password">Password:</label>
          <input type="password" id="password" name="password">
          
          <label for="password-confirm">Confirm Password:</label>
          <input type="password" id="password-confirm" name="password_confirmation">
        </fieldset>

        <button type="submit">Sign Up</button>
      </form>
@endsection
