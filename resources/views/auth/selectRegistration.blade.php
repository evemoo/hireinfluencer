@extends('layouts.app')

@section('content')

	<div class="container">
		<a href="{{ route('register.influencer') }}"><span class="col-md-4 col-md-offset-2"><button class="btn btn-lg btn-primary">as Influencer</button></span></a>
		<a href="{{ route('register.brand') }}"><span class="col-md-4"><button class="btn btn-lg btn-info">as Brand</button></span></a>
	</div>

@endsection