<div class="header-nav-bar">
		<div class="container">

			<!-- Logo -->
			<div class="css-table logo">
				<div class="css-table-cell">
					<a href="index.html">
						<img src="img/header-logo.png" alt="">
					</a> <!-- end .logo -->
				</div>
			</div>

			<!-- Mobile Menu Toggle -->
			<a href="#" id="mobile-menu-toggle"><span></span></a>
			<!-- Primary Nav -->
			<nav>
				<ul class="primary-nav">
					<li class="{{ Request::is('/')?'active':'' }}">
						<a href="{{ route('frontend.home') }}">Home</a>
					</li>
					<li class="{{ Request::is('jobs*')?'active':'' }}">
						<a href="{{ route('frontend.job') }}">Jobs</a>
					</li>
					<li class="has-submenu">
						<a href="candidates.html">Candidates</a>
						<ul>
							<li><a href="candidates.html">Candidates Listings</a></li>
							<li><a href="candidates-sidebar.html">Candidates Listings Sidebar</a></li>
							<li><a href="candidates-single.html">Candidates Details</a></li>
						</ul>
					</li>
					<li class="has-submenu">
						<a href="about-us.html">About Us</a>
						<ul>
							<li><a href="partners.html">Partners</a></li>
							<li><a href="contact-us.html">Contact Us</a></li>
						</ul>
					</li>
					<li><a href="register.html">Register</a></li>
					<li><a href="shortcodes.html">Shortcodes</a></li>
				</ul>
			</nav>
		</div> <!-- end .container -->

		<div id="mobile-menu-container" class="container">
			<div class="login-register"></div>
			<div class="menu"></div>
		</div>
	</div> <!-- end .header-nav-bar -->