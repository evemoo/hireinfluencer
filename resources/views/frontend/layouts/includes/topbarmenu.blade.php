<div class="header-top-bar">
	<div class="container">

		@if(!auth()->check())
		<!-- Header Register -->
		<div class="header-register">
			<a href="#" class="btn btn-link">Register</a>
			<div>
				<form action="#">
					<input type="text" class="form-control" placeholder="Username">
					<input type="email" class="form-control" placeholder="Email">
					<input type="password" class="form-control" placeholder="Password">
					<input type="submit" class="btn btn-default" value="Register">
				</form>
			</div>
		</div> <!-- end .header-register -->

		<!-- Header Login -->
		<div class="header-login">
			<a href="#" class="btn btn-link">Login</a>
			<div>
				<form action="{{ route('login') }}" method="POST">
					{{ csrf_field() }}
					<input type="text" class="form-control" placeholder="email">
					<input type="password" class="form-control" placeholder="Password">
					<input type="submit" class="btn btn-default" value="Login">
					<a href="{{ route('password.request') }}" class="btn btn-link">Forgot Password?</a>
				</form>
			</div>
		</div> <!-- end .header-login -->
		@else
		<div class="header-login">
			<a href="javascript:;" class="btn btn-link">{{ auth()->user()->username }}</a>
			<div style="width: 150px;">
				<ul style="list-style: none;">
					<a href="{{ route('backend.dashboard') }}"><li>Profile</li></a>
					<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><li>Logout</li></a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
				</ul>
			</div>
		</div>
		@endif

	</div> <!-- end .container -->
</div> <!-- end .header-top-bar -->