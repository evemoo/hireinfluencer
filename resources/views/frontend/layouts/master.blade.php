<!doctype html>
<html lang="en">

@include('frontend.layouts.partials.head')

<body>
<div id="main-wrapper">

	@include('frontend.layouts.partials.header')

	@yield('content')

	@include('frontend.layouts.partials.footer')

</div> <!-- end #main-wrapper -->

@include('frontend.layouts.partials.scripts')

</body>
</html>