<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
<script src="{{ asset('frontend/js/maplace.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.ba-outside-events.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.responsive-tabs.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.fitvids.js') }}"></script>
<script src="{{ asset('frontend/js/jquery-ui-1.10.4.custom.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.inview.min.js') }}"></script>
<script src="{{ asset('frontend/js/script.js') }}"></script>