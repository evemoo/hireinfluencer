<header id="header" class="header-style-1">
	@include('frontend.layouts.includes.topbarmenu')

	@include('frontend.layouts.includes.navbarmenu')
		

	@if(Request::is('/'))
	<div class="header-search-bar">
		<div class="container">
			<form>
				<div class="basic-form clearfix">
					<a href="#" class="toggle"><span></span></a>

					<div class="hsb-input-1">
						<input type="text" class="form-control" placeholder="I'm looking for a ...">
					</div>

					<div class="hsb-text-1">in</div>

					<div class="hsb-container">
						<div class="hsb-input-2">
							<input type="text" class="form-control" placeholder="Location">
						</div>

						<div class="hsb-select">
							<select class="form-control">
								<option value="0">Select Category</option>
								<option value="">Category</option>
								<option value="">Category</option>
								<option value="">Category</option>
								<option value="">Category</option>
							</select>
						</div>
					</div>

					<div class="hsb-submit">
						<input type="submit" class="btn btn-default btn-block" value="Search">
					</div>
				</div>

				<div class="advanced-form">
					<div class="container">
						<div class="row">
							<label class="col-md-3">Distance</label>

							<div class="col-md-9">
								<div class="range-slider">
									<div class="slider" data-min="1" data-max="200" data-current="100"></div>
									<div class="last-value"><span>100</span> km</div>
								</div>
							</div>
						</div>

						<div class="row">
							<label class="col-md-3">Rating</label>

							<div class="col-md-9">
								<div class="range-slider">
									<div class="slider" data-min="1" data-max="100" data-current="20"></div>
									<div class="last-value">&gt; <span>20</span> %</div>
								</div>
							</div>
						</div>

						<div class="row">
							<label class="col-md-3">Days Published</label>

							<div class="col-md-9">
								<div class="range-slider">
									<div class="slider" data-min="1" data-max="60" data-current="30"></div>
									<div class="last-value">&lt; <span>30</span></div>
								</div>
							</div>
						</div>

						<div class="row">
							<label class="col-md-3">Location</label>

							<div class="col-md-9">
								<input type="text" class="form-control" placeholder="Switzerland">
							</div>
						</div>

						<div class="row">
							<label class="col-md-3">Industry</label>

							<div class="col-md-9">
								<select class="form-control">
									<option value="">Select Industry</option>
									<option value="">Option 1</option>
									<option value="">Option 2</option>
									<option value="">Option 3</option>
									<option value="">Option 4</option>
									<option value="">Option 5</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div> <!-- end .header-search-bar -->

	<div class="header-banner">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="header-banner-box register">
						<div class="counter-container">
							<ul class="counter clearfix">
								<li class="zero">0</li>
								<li>3</li>
								<li>5</li>
								<li>1</li>
								<li>0</li>
								<li>9</li>
							</ul>

							<div><span>Jobs</span></div>
						</div>

						<a href="#" class="btn btn-default">Register Now</a>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="header-banner-box post-job">
						<img src="img/verified.png" alt="">

						<a href="#" class="btn btn-red">Post a Job</a>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end .header-banner -->
	@endif

	@if(Request::is('jobs'))
	<div class="header-page-title">
		<div class="container">
			<h1>Available Jobs <small>(350)</small></h1>

			<ul class="breadcrumbs">
				<li><a href="#">Home</a></li>
				<li><a href="#">Jobs</a></li>
			</ul>
		</div>
	</div>
	@endif
</header> <!-- end #header -->