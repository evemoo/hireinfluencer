@extends('frontend.layouts.master')

@section('content')

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 page-sidebar">
					<aside>
						<div class="widget sidebar-widget white-container candidates-single-widget">
							<div class="widget-content">

								<h5 class="bottom-line">Job Details</h5>

								<table>
									<tbody>
										<tr>
											<td>ID</td>
											<td>{{ $data->post_id }}</td>
										</tr>

										<tr>
											<td>Location</td>
											<td>Australia</td>
										</tr>

										<tr>
											<td>Industry</td>
											<td>IT/Computers</td>
										</tr>

										<tr>
											<td>Type</td>
											<td>Employer</td>
										</tr>

										<tr>
											<td>Role</td>
											<td>Front-end Developer</td>
										</tr>

										<tr>
											<td>Joining Date</td>
											<td>-</td>
										</tr>

										<tr>
											<td>Employment Status</td>
											<td>-</td>
										</tr>

										<tr>
											<td>Monthly Salary</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>

								<h5 class="bottom-line">Preffered Candidates</h5>

								<table>
									<tbody>
										<tr>
											<td>Career Level</td>
											<td>Entry Level</td>
										</tr>

										<tr>
											<td>Years of Experience</td>
											<td>-</td>
										</tr>

										<tr>
											<td>Residence Location</td>
											<td>Australia</td>
										</tr>

										<tr>
											<td>Gender</td>
											<td>-</td>
										</tr>

										<tr>
											<td>Nationality</td>
											<td>-</td>
										</tr>

										<tr>
											<td>Degree</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</aside>
				</div> <!-- end .page-sidebar -->

				<div class="col-sm-8 page-content">
					<div class="clearfix mb30 hidden-xs">
						<a href="#" class="btn btn-gray pull-left">Back to Listings</a>
						<div class="pull-right">
							<a href="#" class="btn btn-gray">Previous</a>
							<a href="#" class="btn btn-gray">Next</a>
						</div>
					</div>

					<div class="jobs-item jobs-single-item">
						<div class="thumb">{!! $data->image() !!}</div>
						<div class="clearfix visible-xs"></div>
						<div class="date">27 <span>Jun</span></div>
						<h6 class="title"><a href="#">{{ $data->post_title }}</a></h6>
						<span class="meta">Envato, Sydney, AU</span>

						<ul class="top-btns">
							<li><a href="#" class="btn btn-gray fa fa-star"></a></li>
						</ul>

						<p>{{ $data->description }}</p>

						{{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, nihil, dolores, culpa ullam vero ipsum placeat accusamus nemo ipsa cupiditate id molestiae consectetur quae pariatur repudiandae vel ex quaerat nam iusto aliquid laborum quia adipisci aut ut impedit obcaecati nisi deleniti tempore maxime sequi fugit reiciendis libero quo. Rerum, assumenda.</p>

						<ul>
							<li>Lorem ipsum dolor sit amet.</li>
							<li>Reiciendis laborum mollitia ad vel?</li>
							<li>Velit voluptates neque perspiciatis suscipit?</li>
							<li>Facere, sequi quo iste odio.</li>
							<li>A culpa libero tempora ut.</li>
						</ul>

						<div class="fitvidsjs">
							<div class="fluid-width-video-wrapper" style="padding-top: 56.2%;"><iframe src="//player.vimeo.com/video/24456787" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" id="fitvid674793" frameborder="0"></iframe></div>
						</div>

						<h5>Required Skills</h5>

						<div class="progress-bar toggle" data-progress="60">
							<a href="#" class="progress-bar-toggle"></a>
							<h6 class="progress-bar-title">Web Design</h6>
							<div class="progress-bar-inner"><span></span></div>
							<div class="progress-bar-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
							</div>
						</div>

						<div class="progress-bar toggle" data-progress="60">
							<a href="#" class="progress-bar-toggle"></a>
							<h6 class="progress-bar-title">Development</h6>
							<div class="progress-bar-inner"><span></span></div>
							<div class="progress-bar-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
							</div>
						</div>

						<div class="progress-bar toggle" data-progress="60">
							<a href="#" class="progress-bar-toggle"></a>
							<h6 class="progress-bar-title">UI/UX</h6>
							<div class="progress-bar-inner"><span></span></div>
							<div class="progress-bar-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, asperiores.
							</div>
						</div>

						<h5>Additional Requirements</h5>

						<ul class="additional-requirements clearfix">
							<li>Work Permit</li>
							<li>5 Years Experience</li>
							<li>MBA</li>
							<li>Magento Certified</li>
							<li>Perfect Written &amp; Spoken English</li>
						</ul> --}}

						<hr>

						<div class="clearfix">
							<a href="#" class="btn btn-default pull-left">Make a Bid</a>

							<ul class="social-icons pull-right">
								<li><span>Share</span></li>
								<li><a href="#" class="btn btn-gray fa fa-facebook"></a></li>
								<li><a href="#" class="btn btn-gray fa fa-twitter"></a></li>
								<li><a href="#" class="btn btn-gray fa fa-google-plus"></a></li>
							</ul>
						</div>
					</div>

					<div class="title-lines">
						<h3 class="mt0">About the Brand</h3>
					</div>

					<div class="about-candidate-item">
						<div class="thumb"><img src="img/content/face-9.png" alt=""></div>

						<h6 class="title"><a href="#">{{ $data->user->full_name }}</a></h6>
						<span class="meta">24 Years Old - Sydney, AU</span>

						<ul class="social-icons clearfix">
							<li><a href="#" class="btn btn-gray fa fa-facebook"></a></li>
							<li><a href="#" class="btn btn-gray fa fa-twitter"></a></li>
							<li><a href="#" class="btn btn-gray fa fa-google-plus"></a></li>
							<li><a href="#" class="btn btn-gray fa fa-dribbble"></a></li>
							<li><a href="#" class="btn btn-gray fa fa-pinterest"></a></li>
							<li><a href="#" class="btn btn-gray fa fa-linkedin"></a></li>
						</ul>

						<ul class="list-unstyled">
							<li><strong>Tel:</strong> (123) 123-4567</li>
							<li><strong>Email:</strong> <a href="#">{{ $data->user->email }}</a></li>
						</ul>

						<a href="#" class="btn btn-default">Send Message</a>
					</div>
				</div> <!-- end .page-content -->
			</div>
		</div> <!-- end .container -->
	</div>

@endsection