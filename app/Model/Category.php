<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id', 'title', 'slug', 'status'];

    public function status()
    {
    	$status = $this->status;
    	if($status == 1)
    		return '<span class="label label-sm label-success"> Active </span>';
    	elseif($status == 0)
    		return '<span class="label label-sm label-warning"> Inactive </span>';
    }

    public function child()
    {
    	$parent_id = $this->id;
    	$child = Category::where('parent_id', $parent_id)->get();
    	return $child;
    }
}
