<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'post_title', 'slug', 'post_id', 'description', 'image', 'category_id', 'email', 'bid_expiry', 'currency', 'budget_low', 'budget_high', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function publish()
    {
    	$data = $this->status;
    	if($data == 1)
    		return '<span class="label label-sm label-success"> Published </span>';
    	elseif($data == 0)
    		return '<span class="label label-sm label-warning"> Draft </span>';
    }

    public function humanDate($date)
    {
    	$return = date_format($date, 'M d, Y');
    	return $return;
    }

    public function budget()
    {
    	$currency = $this->currency;
    	$budget_low = $this->budget_low;
    	$budget_high = $this->budget_high;
    	$budget = $currency.$budget_low.' - '.$currency.$budget_high;
    	
    	return $budget;
    }

    public function image()
    {
    	$image = $this->image;
    	if($image)
    	{
    		$url = asset('images/post/'.$image);
    		return '<img src="'.$url.'" alt="">';
    	}
    	else
    	{
    		$url = asset('images/common/no-image.jpg');
    		return '<img src="'.$url.'" alt="">';
    	}
    }
}
