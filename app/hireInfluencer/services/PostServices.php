<?php

namespace App\hireInfluencer\services;

/**
* 
*/

use App\Model\Post;
use App\Model\Category;
use Illuminate\Http\Request;

class PostServices
{
	public function findBySlug($slug)
	{
		$data['row'] = Post::where('slug', $slug)->firstOrFail();
		return $data;
	}

	public function getPosts()
	{
		$data['rows'] = Post::orderBy('id', 'desc')->get();
		return $data;
	}

	public function uploadImage(Request $request)
	{
		$image = $request->file('image');
		$imageName = time().'.'.$image->getClientOriginalExtension();
		$folderPath = public_path('/images/post');
		$image->move($folderPath, $imageName);

		return $imageName;
	}

	public function parentChild()
	{
		$data = [];
		$parents = Category::where('parent_id', 0)->get();
		foreach ($parents as $key => $parent) {
			if($this->hasChild($parent->id) == true)
				$data[$parent->title] = Category::where('parent_id', $parent->id)->pluck('title', 'id')->toArray();
		}
		return $data;
	}

	protected function hasChild($id)
	{
		$child = Category::where('parent_id', $id)->first();
		if($child)
			return true;
		else
			return false;
	}
}