<?php

namespace App\hireInfluencer\services;

/**
* 
*/

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryServices
{
	public function findBySlug($slug)
	{
		$data['row'] = Category::where('slug', $slug)->firstOrFail();
		return $data;
	}

	public function getCategories()
	{
		$data['rows'] = Category::where('parent_id', 0)->orderBy('title', 'asc')->get();
		return $data;
	}

	public function getParents($slug = '')
	{
		$data['parents'] = Category::where('parent_id', 0)->where('slug', '!=', $slug)->pluck('title', 'id')->toArray();

		asort($data['parents']);

		return $data;
	}
}