<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
	public function __construct()
	{
	    $this->middleware('guest');
	}

    public function influencer()
    {
    	return view('auth.register');
    }

    public function brand()
    {
    	return view('auth.register');
    }
}
