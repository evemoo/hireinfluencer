<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BaseController;
use App\hireInfluencer\services\CategoryServices;
use App\Model\Category;

class CategoryController extends BaseController
{
    protected $panel = 'Category';
	protected $base_route = 'backend.category';
	protected $view_path = 'backend.category';

    private $services;

	public function __construct(CategoryServices $services)
	{
		parent::__construct();
        $this->services = $services;
	}

    public function index()
    {
        $data = [];
        $data = $this->services->getCategories();
    	return view(parent::loadDataToView($this->view_path.'.index'), compact('data'));
    }

    public function create()
    {
    	$data = [];
    	$data = $this->services->getParents();
		return view(parent::loadDataToView($this->view_path.'.create'), compact('data'));
    }

    public function store(Request $request)
    {
    	$slug = str_slug($request['title'], '-');

    	if($request['parent_id'] == null)
    		$parent_id = '0';
    	else
    		$parent_id = $request['parent_id'];

        $create = Category::create([
            'title'        => $request['title'],
            'slug'         => $slug,
            'parent_id'    => $parent_id,
            'status'       => $request['status'],
        ]);

        if($create)
            session()->flash('message', 'Category has been successfully stored!');

        return redirect()->route($this->base_route);
    }

    public function view($slug)
    {
        $data = $this->services->findBySlug($slug);
        $data = array_merge($data, $this->services->getPosts());
        return view(parent::loadDataToView($this->view_path.'.view'), compact('data'));
    }

    public function edit($slug)
    {
    	$data = [];
        $data = $this->services->findBySlug($slug);
    	$data = array_merge($data, $this->services->getParents($slug));
        return view(parent::loadDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $slug)
    {
        $data = $this->services->findBySlug($slug);

        if($data['row']->title == $request->title)
        {
            $slug = $data['row']->slug;
        }
        else
        {
            $slug = str_slug($request['post_title'], '-');
        }

        if($request['parent_id'] == null)
    		$parent_id = '0';
    	else
    		$parent_id = $request['parent_id'];

        $update = $data['row']->update([
            'title'        => $request['title'],
            'slug'         => $slug,
            'parent_id'    => $parent_id,
            'status'       => $request['status'],
        ]);

        if($update)
            session()->flash('message', 'Your post has been successfully updated!');

        return redirect()->route($this->base_route);
    }

    public function delete(Request $request)
    {
        //dd($request);
        $response = [];
        $data = $this->services->findBySlug($request->slug);
        $data['row']->delete();
        return response()->json(json_encode($response));
    }
}
