<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BaseController;

class DashboardController extends BaseController
{
	protected $panel = 'Dashboard';
	protected $base_route = 'backend.dashboard';
	protected $view_path = 'backend.dashboard';

	public function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
    	return view(parent::loadDataToView($this->view_path.'.index'));
    }
}
