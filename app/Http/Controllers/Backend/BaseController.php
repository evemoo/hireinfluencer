<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function __construct()
    {
        
    }

    protected function loadDataToView($view_path)
    {
        view()->composer($view_path, function ($view) use ($view_path) {

            $view->with('view_path', $this->view_path);
            $view->with('base_route', $this->base_route);
            $view->with('panel', $this->panel);
        });

        return $view_path;
    }
}
