<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\BaseController;
use App\Model\Post;
use App\hireInfluencer\services\PostServices;

class PostController extends BaseController
{
    protected $panel = 'Post';
	protected $base_route = 'backend.post';
	protected $view_path = 'backend.post';

    private $services;

	public function __construct(PostServices $services)
	{
		parent::__construct();
        $this->services = $services;
	}

    public function index()
    {
        $data = [];
        $data = $this->services->getPosts();
    	return view(parent::loadDataToView($this->view_path.'.index'), compact('data'));
    }

    public function create()
    {
        $data = $this->services->parentChild();
		return view(parent::loadDataToView($this->view_path.'.create'), compact('data'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $uniqId = $this->uniq();
        $slug = str_slug($request['post_title'], '-').'-'.$uniqId;

        $imageName = $this->services->uploadImage($request);

        $create = Post::create([
            'user_id'           => auth()->user()->id,
            'post_title'        => $request['post_title'],
            'slug'              => $slug,
            'post_id'           => $uniqId,
            'description'       => $request['description'],
            'email'             => $request['email'],
            'image'             => $imageName,
            'category_id'       => $request['category_id'],
            'bid_expiry'        => $request['bid_expiry'],
            'currency'          => $request['currency'],
            'budget_low'        => $request['budget_low'],
            'budget_high'       => $request['budget_high'],
            'status'            => $request['status'],
        ]);

        if($create)
            session()->flash('message', 'Your post has been successfully stored!');

        return redirect()->route($this->base_route);
    }

    public function view($slug)
    {
        $data = $this->services->findBySlug($slug);
        $data = array_merge($data, $this->services->getPosts());
        return view(parent::loadDataToView($this->view_path.'.view'), compact('data'));
    }

    public function edit($slug)
    {
        $data = $this->services->findBySlug($slug);
        return view(parent::loadDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $slug)
    {
        $data = $this->services->findBySlug($slug);

        if($data['row']->post_title == $request->post_title)
        {
            $slug = $data['row']->slug;
        }
        else
        {
            $slug = str_slug($request['post_title'], '-').'-'.$data['row']->post_id;
        }

        $update = $data['row']->update([
            'post_title'        => $request['post_title'],
            'slug'              => $slug,
            'description'       => $request['description'],
            'email'             => $request['email'],
            'category_id'       => $request['category_id'],
            'bid_expiry'        => $request['bid_expiry'],
            'budget_low'        => $request['budget_low'],
            'budget_high'       => $request['budget_high'],
            'status'            => $request['status'],
        ]);

        if($update)
            session()->flash('message', 'Your post has been successfully updated!');

        return redirect()->route($this->base_route);
    }

    public function delete(Request $request)
    {
        //dd($request);
        $response = [];
        $data = $this->services->findBySlug($request->slug);
        $data['row']->delete();
        return response()->json(json_encode($response));
    }

    //helper functions
    protected function uniq()
    {
        $key = rand(100000, 999999);
        $check = Post::where('post_id', $key)->first();
        if($check)
            uniq();
        else
            return $key;
    }
}
