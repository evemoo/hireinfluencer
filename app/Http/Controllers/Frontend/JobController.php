<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Post;

class JobController extends Controller
{
    public function index()
    {
    	$data = [];
    	$data = Post::where('status', 1)->get();
    	return view('frontend.job.index', compact('data'));
    }

    public function view($slug)
    {
    	$data = [];
    	$data = Post::where('slug', $slug)->where('status', 1)->firstOrFail();
    	return view('frontend.job.view', compact('data'));
    }
}
