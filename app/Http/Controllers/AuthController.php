<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | max:255',
            'email' => 'required | email | unique:users',
            'password' => 'required | between:6,25 | confirmed'
        ]);

        $request->request->add([
            'full_name' => $request->name,
            'username' => $request->email
        ]);


        $user = User::create($request->all());
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return response()->json([
            'registered' => true
        ]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required | between:6,25'
        ]);

        $user = User::where('email', $request->get('email'))->first();
        if ($user && Hash::check($request->get('password'), $user->password)) {

            $user->api_token = str_random(60);
            $user->save();

            return response()->json([
                'authenticated' => true,
                'api_token' => $user->api_token,
                'user_id' => $user->id
            ]);

        }

        return response()->json([
            'email' => ['Provided email and password does not match!']
        ], 422);

    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->api_token  = null;
        $user->save();

        return response()->json([
            'logged_out' => true
        ]);
    }
}
