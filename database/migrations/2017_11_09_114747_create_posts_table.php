<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->Unsignedinteger('user_id');
            $table->string('post_title');
            $table->string('slug');
            $table->unsignedInteger('post_id');
            $table->unsignedInteger('category_id');
            $table->text('description');
            $table->string('email', 100)->nullable();
            $table->string('image')->nullable();
            $table->dateTime('bid_expiry');
            $table->string('currency');
            $table->Unsignedinteger('budget_low');
            $table->Unsignedinteger('budget_high');
            $table->boolean('status');
            $table->timestamps();
        });

        DB::update("ALTER TABLE posts AUTO_INCREMENT = 135615;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
