<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();

        DB::table('users')->insert([
            'full_name' => 'EveMoo Tech',
            'username' => 'EM_TECH',
            'email' => 'evemoo@gmail.com',
            'password' => bcrypt('evemoo'),
            'avatar' => '',
        ]);

        DB::table('users')->insert([
            'full_name' => 'John Influencer',
            'username' => 'j_influencer',
            'email' => 'j_influencer@gmail.com',
            'password' => bcrypt('j_influencer'),
            'avatar' => '',
        ]);

        DB::table('users')->insert([
            'full_name' => 'Brand Doe',
            'username' => 'brand_d',
            'email' => 'brand_d@gmail.com',
            'password' => bcrypt('brand_d'),
            'avatar' => '',
        ]);
    }
}
