<?php

Route::group(['middleware' => 'web', 'prefix' => 'userpanel', 'namespace' => 'Modules\UserPanel\Http\Controllers'], function()
{
    Route::get('/', 'UserPanelController@index');

});
