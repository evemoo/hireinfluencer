@extends($_module.'::layouts.auth')

@section('css')
    <link href="{{ asset(config('evemoo.assets_path.bower_components').'materialize-social/materialize-social.css') }}" rel="stylesheet" type="text/css">
    @endsection

@section('content')

    <!-- BEGIN : LOGIN PAGE 5-1 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            @include($_module.'::common.auth_left_section')

            <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                <div class="login-content">
                    <h1>{{ __($_trans_path.'Hireinfluencer-Influencer-Login') }}</h1>
                    <p> {{ __($_trans_path.'Hireinfluencer-Influencer-Login-Text') }} </p>

                    @include($_view_path.'.index.login_form')
                    @include($_view_path.'.index.forget_password_form')

                    <hr/>

                    <div style="text-align: center;">
                        <a class="waves-effect waves-light btn social facebook"><i class="fa fa-facebook"></i> Sign in with facebook</a>
                        <a class="waves-effect waves-light btn social google"><i class="fa fa-google"></i> Sign in with google</a>
                        <a class="waves-effect waves-light btn social instagram"><i class="fa fa-instagram"></i> Sign in with instagram</a>
                    </div>

                    <hr/>

                    <div style="text-align: center;">
                        <a href="{{ route($_module.'.register') }}" class="btn green mt-clipboard"><i class="icon-login"></i>&nbsp;&nbsp;Sign Up</a>
                    </div>

                </div>

                @include($_module.'::common.auth_footer')
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-1 -->

    @endsection