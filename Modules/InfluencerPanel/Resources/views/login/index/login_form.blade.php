{!! Form::open(['route' => $_base_route.'.post', 'class' => 'login-form']) !!}
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span>{{ __($_trans_path.'validation-alert-message') }} </span>
    </div>
    <div class="row">
        <div class="col-xs-6">
            {!! Form::email('email', null, [
                'class' => 'form-control form-control-solid placeholder-no-fix form-group',
                'autocomplete' => 'off',
                'placeholder' => __($_trans_path.'Email'),
                'require'
                ]) !!}
        </div>
        <div class="col-xs-6">
            <input type="password" name="password" class="form-control form-control-solid placeholder-no-fix form-group"
                   autocomplete="off" placeholder="{{ __($_trans_path.'Password') }}" required>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="rem-password">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1" /> {{ __($_trans_path.'Remember-Me') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-sm-8 text-right">
            <div class="forgot-password">
                <a href="javascript:;" id="forget-password" class="forget-password">{{ __($_trans_path.'Forgot-Password?') }}</a>
            </div>
            <button class="btn green" type="submit">{{ __($_trans_path.'Sign-In') }}</button>
        </div>
    </div>
{!! Form::close() !!}