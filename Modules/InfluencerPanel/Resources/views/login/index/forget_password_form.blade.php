<!-- BEGIN FORGOT PASSWORD FORM -->
{!! Form::open(['route' => $_base_route.'.forgot-password.post', 'class' => 'forget-form', 'style' => 'display:none']) !!}
    <h3 class="font-green">{{ __($_trans_path.'Forgot-Password?') }}</h3>
    <p> {{ __($_trans_path.'Forgot-Password?-Text') }} </p>
    <div class="form-group">
        {!! Form::email('email', null, [
           'class' => 'form-control form-control-solid placeholder-no-fix form-group',
           'autocomplete' => 'off',
           'placeholder' => __($_trans_path.'Email'),
           'require'
        ]) !!}
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn green btn-outline">{{ __($_trans_path.'Back') }}</button>
        <button type="submit" class="btn btn-success uppercase pull-right">{{ __($_trans_path.'Submit') }}</button>
    </div>
{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->