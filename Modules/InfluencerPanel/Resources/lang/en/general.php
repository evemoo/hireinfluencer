<?php
return [
    'register' => [
        'page_title' => 'Registration :: HireInfluencer.com',
        'Hireinfluencer-Influencer-Registration' => 'Hireinfluencer Influencer Registration',
        'Hireinfluencer-Influencer-Registration-Text' => 'Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing.',
        'Brand-Name' => 'Brand Name',
        'Brand-Url' => 'Brand Url',
        'Full-Name' => 'Full Name',
        'Username' => 'Username',
        'Email' => 'Email',
        'Contact' => 'Contact',
        'Password' => 'Password',
        'Confirm-Password' => 'Confirm Password',
        'Has-Login?' => 'Has Login?',
        'Sign-Up' => 'Sign Up',
        'validation-alert-message' => 'Some fields remained blank or filled with invalid data.',
        'Message' => 'Message',
    ],
    'login' => [
        'page_title' => 'Login :: HireInfluencer.com',
        'Hireinfluencer-Influencer-Login' => 'Hireinfluencer Influencer Login',
        'Hireinfluencer-Influencer-Login-Text' => 'Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing.',
        'Email' => 'Email',
        'Password' => 'Password',
        'Remember-Me' => 'Remember Me',
        'Forgot-Password?' => 'Forgot Password?',
        'Sign-In' => 'Sign In',
        'Forgot-Password?-Text' => 'Enter your e-mail address below to reset your password.',
        'Back' => 'Back',
        'Submit' => 'Submit',
        'validation-alert-message' => 'Enter email and password.'
    ],
];