<?php

Route::group([
    'middleware' => 'web', 'prefix' => 'influencerpanel', 'as' => 'influencerpanel.',
    'namespace' => 'Modules\InfluencerPanel\Http\Controllers'
], function()
{
    Route::get('register',                              ['as' => 'register',                            'uses' => 'RegisterController@register']);
    Route::post('register',                             ['as' => 'register.post',                       'uses' => 'RegisterController@registerPost']);

    Route::get('login',                                 ['as' => 'login',                               'uses' => 'LoginController@login']);
    Route::post('login',                                ['as' => 'login.post',                          'uses' => 'LoginController@loginPost']);
    Route::post('forgot-password',                      ['as' => 'login.forgot-password.post',          'uses' => 'LoginController@forgotPassword']);

});
