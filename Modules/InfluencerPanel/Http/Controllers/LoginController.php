<?php

namespace Modules\InfluencerPanel\Http\Controllers;


class LoginController extends BaseController
{
    protected $panel = 'login';

    public function login()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.index'));
    }

}
