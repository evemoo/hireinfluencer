<?php

namespace Modules\InfluencerPanel\Http\Controllers;


class RegisterController extends BaseController
{
    protected $panel = 'register';

    public function register()
    {
        return view(parent::loadDefaultDataToView($this->view_path.'.index'));
    }

}
