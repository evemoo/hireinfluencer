<?php

namespace Modules\InfluencerPanel\Http\Controllers;

use Illuminate\Routing\Controller;
use View;

class BaseController extends Controller
{
    protected $module_alias = 'influencerpanel';
    protected $base_route;
    protected $view_path;
    protected $trans_path;

    public function __construct()
    {
        $this->base_route = $this->module_alias.'.'.$this->panel;
        $this->view_path = $this->module_alias.'::'.$this->panel;
        $this->trans_path = $this->module_alias.'::general.'.$this->panel.'.';
    }

    protected function loadDefaultDataToView($view_path)
    {
        View::composer($view_path, function ($view) {

            $view->with('_module', $this->module_alias);
            $view->with('_base_route', $this->base_route);
            $view->with('_view_path', $this->view_path);
            $view->with('_trans_path', $this->trans_path);
            $view->with('_asset_path', config($this->module_alias.'.asset_path'));

        });

        return $view_path;
    }
}
